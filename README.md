# React Practice

## Leacture 1
1.What is Emmet?


    Generating HTML Skeleton  --> !
    Nested elements -->To generate nested we will use ‘>’ operator --> ul>li
    What about classes and Id --> div#main.container.responsive ->you can specify Id by using ‘#’ and classes by using ‘.’
     ```
         div#main.container
     ```
    Generating Siblings --> To generate sibling use ‘+’ operator --> header+div+footer
    lorem100

2. Difference between a Library and Framework?


    A library is a collection of pre-written code that you can use in your own code to perform specific tasks. A framework is a set of rules or guidelines for building and organizing code.

    For example, React is a JavaScript library for building user interfaces. It provides a set of tools for creating reusable UI components and managing the state of those components.

    On the other hand, Angular is a full-fledged JavaScript framework for building web applications. It provides a complete set of tools for building a web application, including a router for handling navigation, a dependency injectionor for managing dependencies, and a set of built-in directives for building UI components.

    In general, libraries are more focused on specific tasks, whereas frameworks provide a more comprehensive set of tools for building an application.

3. What is CDN?


    A CDN is a network of servers that distributes content from an “origin” server throughout the world by caching content close to where each end user is accessing the internet via a web-enabled device. The content they request is first stored on the origin server and is then replicated and stored elsewhere as needed.

4. Why is React known as React?


    React is a JavaScript library for building user interfaces, and it is known as React because that is the name that was chosen by its creators. The name "React" was chosen to reflect the library's purpose, which is to allow developers to build reactive and responsive user interfaces.

    React was created by Facebook software engineer Jordan Walke, who was inspired by functional programming concepts and the idea of building user interfaces as a function of the underlying data. Walke named the library "React" to emphasize its focus on declarative, reactive programming, which allows developers to describe the desired state of their user interfaces and have the library automatically update the interface to match that state.

5.What is crossorigin in script tag?
     ```
           https://youtube.com/watch?v=tcLW5d0KAYE&feature=shares
     ```

    The crossorigin attribute is used in the HTML script tag to specify how the browser should handle the cross-origin request for the script file.

    The value of the crossorigin attribute can be either anonymous or use-credentials.

    If the value is anonymous, the browser will make a request for the script file with the Origin header set to the current origin (the origin of the page that is making the request). The server hosting the script file is expected to return appropriate CORS (Cross-Origin Resource Sharing) headers to allow the browser to execute the script. However, the server will not receive any credentials (cookies, HTTP authentication, or client-side SSL certificates) with the request.

    If the value is use-credentials, the browser will make a request for the script file with the Origin header set to the current origin and will also include any credentials (cookies, HTTP authentication, or client-side SSL certificates) with the request. The server hosting the script file is expected to return appropriate CORS headers to allow the browser to execute the script.

    Here is an example of the script tag with the crossorigin attribute:

    ```
    <script src="https://example.com/script.js" crossorigin="anonymous"></script>
    ```
    By default, the crossorigin attribute is not set and the browser will not include the Origin header or any credentials with the request. This means that the server hosting the script file is expected to allow all origins to access the script file, or the browser will not execute the script.

6.What is diference between React and ReactDOM


    React is a JavaScript library for building user interfaces. It is designed to make it easy to build interactive, dynamic user interfaces in a declarative way, meaning you describe what you want the interface to look like and React takes care of the rest.

    ReactDOM is a separate library that provides the means to render a React application to the DOM (Document Object Model). It provides a set of methods that allow you to manipulate the DOM in order to display the components of your React application.

    In other words, React is a tool for building reusable UI components, and ReactDOM is a tool for rendering those components to the DOM. They are typically used together, but they serve different purposes.

7.What is difference between react.development.js and react.production.js files via CDN?


    The react.development.js file is the development version of the React JavaScript library, while the react.production.js file is the production version of the library. These files are available as part of the React library when you include it in your project using a CDN (Content Delivery Network).

    The main difference between the development and production versions is in the way they are built and the level of debugging information they provide.

    The development version of React is built with extra debugging information and includes helpful warnings for common mistakes that developers might make. It is meant to be used during the development process, as it can help you identify and fix issues in your code more easily.

    The production version of React, on the other hand, is optimized for performance and does not include any debugging information. It is meant to be used in a production environment, where the focus is on delivering a fast and reliable experience to users.

    In general, you should use the development version of React during the development process and switch to the production version when deploying your app to a production environment. This will ensure that you have the necessary debugging information while you are working on your app and that your app performs optimally when it is released to users.

8.What is async and defer?
```
https://youtube.com/watch?v=IrHmpdORLu8&feature=shares
```
